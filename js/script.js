$( document ).ready(function() {
  var myMap;
  ymaps.ready(init);

  function init () {
      var myMap = new ymaps.Map('map', {
              center: [64.531265, 40.526833],
              zoom: 16
          }),
      myPlacemark = new ymaps.Placemark([64.531265, 40.526833], { }, {
          // Опции.
          // Необходимо указать данный тип макета.
          iconLayout: 'default#image',
          // Своё изображение иконки метки.
          iconImageHref: 'img/map-marker.png',
          // Размеры метки.
          iconImageSize: [126, 167],
          // Смещение левого верхнего угла иконки относительно
          // её "ножки" (точки привязки).
          iconImageOffset: [-63, -167]
      });

      myMap.controls
      .remove('mapTools')
      .remove('zoomControl')
      .remove('searchControl')
      .remove('typeSelector')
      .remove('geolocationControl')
      .remove('fullscreenControl')
      .remove('rulerControl')
      .remove('trafficControl');
      myMap.controls.add(new ymaps.control.ZoomControl({options: { position: { left: 10, top: 170 }}}));
      myMap.geoObjects.add(myPlacemark);
      myMap.behaviors.disable("scrollZoom");
  }

  $('header nav').singlePageNav({
    offset: $('header').outerHeight(),
    filter: ':not(.disabled)',
    updateHash: true,
  });

  $('.projects .slider').bxSlider({
    pager: false,
    prevText: '',
    nextText: '',
    slideWidth: 900,
    onSliderLoad: function(slideId, elem){
      var classes = $(elem.viewport).parents('.bx-wrapper').attr('class');
      $(elem.viewport).parents('.bx-wrapper').addClass($(elem.viewport).find('.slider').attr('class').split(' ')[1]);
      if ($(elem.viewport).find('.slider').attr('class').split(' ').indexOf('hoa')!=-1){
        $(elem.viewport).parents('.bx-wrapper').show();
      }
      else{
        $(elem.viewport).parents('.bx-wrapper').hide();
      }
    },
  });

  // $('.clients .slider').bxSlider({
  //   minSlides: 3,
  //   maxSlides: 3,
  //   slideWidth: 245,
  //   slideMargin: 80,
  //   moveSlides: 1,
  //   pager: false,
  //   prevText: '',
  //   nextText: '',
  // });

  $('.feedbacks .slider').bxSlider({
    minSlides: 5,
    maxSlides: 5,
    slideWidth: 180,
    moveSlides: 1,
    pager: false,
    prevText: '',
    nextText: '',
  });

  setTimeout(function() {
    $('header nav a, header .address, header .tel').each(function() {
      $(this).text($(this).text())
    });
  }, 600);

  $(window).on("load resize",function(e){
    //$('header').width($(window).width());
  });

  $('#confidential').click(function(e) {
    e.preventDefault();
    $('#confidential-modal').arcticmodal();
  });

  $(".contactform").on("submit", function (e) {
    var form_ya = $(this).data('ya');
    e.preventDefault();
    if ($(this).validationEngine('validate', {promptPosition: "topRight", scroll: false})) {
      var answer = $(this).serialize();
      $.arcticmodal('close');
      this.reset();
      $.ajax({
        type: "POST",
        cache: false,
        data: answer,
        url: "/php/submit.php",
        success: function (data) {          
          $("#success-modal").arcticmodal();
          ga('send', 'event', 'request', 'send');
          yaCounter43246109.reachGoal('request');
        }
      });
      $.ajax({
        type: "POST",
        cache: false,
        data: answer,
        url: "/php/amocrm.php"
      });
    }
    return false;
  });

  $(".contactform input[type=text]").on('keydown', function() {
    $('.formError').animate({
        opacity: 0,
      }, 400, function() {
        $('.formError').remove();
      });
  });

  $('#success-modal .button').click(function() {
    $('#success-modal').arcticmodal('close');
  });

  $('.question .ask-button').click(function(e) {
    e.preventDefault();
    $('#question-modal').arcticmodal();
  });

  $(".modal-call").click(function(e){
    e.preventDefault();
    $("#zvonok-modal").arcticmodal();
  });

  $('.feedbacks .slide').click(function() {
    $('#zoom-modal img').attr('src', $(this).data('big'));
    $('#zoom-modal').arcticmodal();
  });

  $('.spheres .tab').click(function() {
    var sphere = $(this).data('target');

    $('.spheres .tab').removeClass('active');
    $(this).addClass('active');

    var sections = ['.spheres .sphere', '.access .text', '.projects .bx-wrapper', '.clients .slider', '.why-we .reasons'];

    $.each(sections, function( index, value ) {
      $(value).hide();
      $(value+'.'+sphere).show();
    });

    var new_action = $('.access .text:visible').text().toLowerCase();
    new_action = new_action.substr(0, 1).toUpperCase() + new_action.substr(1);
    $('.access form input[name=action]').val(new_action);
  });
});
