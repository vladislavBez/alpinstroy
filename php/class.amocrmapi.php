<?
  /**
  * AMOCRMAPI
  */
  class AMOCRM
  {
    public $subdomain = '';
    public $mail = '';
    public $hash = '';
    public $responsible_user_id = '';

    function __construct($subdomain, $mail, $hash, $responsible_user_id)
    {
      $this->subdomain = $subdomain;
      $this->mail = $mail;
      $this->hash = $hash;
      $this->responsible_user_id = $responsible_user_id;
    }

    public function auth()
    {
      $user=array(
        'USER_LOGIN'=>$this->mail,
        'USER_HASH'=>$this->hash
      );
       
      $link='https://'.$this->subdomain.'.amocrm.ru/private/api/auth.php?type=json';

      $resp = $this->execQuery($link, $user, 'POST'); 
      
      return $resp[0]==null ? array(null, $resp[1]['auth']) : $resp;
    }

    private function validateCode($code)
    {
      $valids=array(
        200=>'OK',
        204=>'OK',
      );
      $errors=array(
        301=>'Moved permanently',
        401=>'Unauthorized',
        400=>'Bad request',
        403=>'Forbidden',
        404=>'Not found',
        500=>'Internal server error',
        502=>'Bad gateway',
        503=>'Service unavailable'
      );
      if(!$valids[$code])
        return $errors[$code] ? array($code, $errors[$code]) : array($code, 'Undescribed error code');
      return $valids[$code];
    }

    public function getContacts($query)
    {
      $link='https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/contacts/list?query='.$query;

      $resp = $this->execQuery($link, null, 'GET'); 
      
      return $resp[0]==null ? array(null, $resp[1]) : $resp;
    }

    public function createContact($name, $phone, $mail)
    {
      $contacts['request']['contacts']['add']=array(
        array(
          'name'=>$name,
          'responsible_user_id'=>$this->responsible_user_id
        )
      );

      if ($phone!=null || $mail!=null){
        $contacts['request']['contacts']['add'][0]['custom_fields'] = array();
        if ($phone!=null)
        {
          $phone_arr = array(
            'id'=>1494825, 
            'values'=>array(
              array(
                'value'=>$phone,
                'enum'=>'WORK'
              )
            )
          );
          array_push($contacts['request']['contacts']['add'][0]['custom_fields'], $phone_arr);
        }
        if ($mail!=null)
        {
          $mail_arr = array(
            'id'=>1494827,
            'values'=>array(
              array(
                'value'=>$mail,
                'enum'=>'WORK',
              )
            )
          );
          array_push($contacts['request']['contacts']['add'][0]['custom_fields'], $mail_arr);
        }   
      }

      $link='https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/contacts/set';

      $resp = $this->execQuery($link, $contacts, 'POST'); 
      
      return $resp[0]==null ? array(null, $resp[1]['contacts']['add']) : $resp;
    }

    public function getAccountInfo()
    {
      $link='https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/accounts/current';

      $resp = $this->execQuery($link, null, 'GET'); 
      
      return $resp[0]==null ? array(null, $resp[1]) : $resp;
    }

    public function createLead($name)
    {
      $leads['request']['leads']['add']=array(
        array(
          'name'=>$name,
          'status_id'=>8232906,
          'responsible_user_id'=>$this->responsible_user_id,
        )
      );

      $link='https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/leads/set';

      $resp = $this->execQuery($link, $leads, 'POST'); 
      
      return $resp[0]==null ? array(null, $resp[1]['leads']['add']) : $resp;      
    }

    public function createNote($lead_id, $text)
    {
      $notes['request']['notes']['add']=array(
        array(
          'element_id'=>$lead_id,
          'element_type'=>2,
          'note_type'=>4,
          'text'=>$text,
          'responsible_user_id'=>$this->responsible_user_id,
        )
      );
      $link='https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/notes/set';

      $resp = $this->execQuery($link, $notes, 'POST'); 
      
      return $resp[0]==null ? array(null, $resp[1]['notes']['add']) : $resp;
    }

    public function updateContact($contact_id, $data=null)
    {
      $date = new DateTime();
      $contacts['request']['contacts']['update']=array(
        array(
          'id'=>$contact_id,
          'last_modified'=>$date->getTimestamp()
        )
      );

      if ($data['name'] && $data['name']!='')
        $contacts['request']['contacts']['update'][0]['name']=$data['name'];

      if ($data['leads'] && $data['leads']!='')
        $contacts['request']['contacts']['update'][0]['linked_leads_id']=$data['leads'];

      if ($data && ($data['phone'] || $data['email'])){
        $contacts['request']['contacts']['update'][0]['custom_fields'] = array();
        if ($data['phone'] && $data['phone']!='')
        {
          $phone_arr = array(
            'id'=>1494825, 
            'values'=>array(
              array(
                'value'=>$data['phone'],
                'enum'=>'WORK'
              )
            )
          );
          array_push($contacts['request']['contacts']['update'][0]['custom_fields'], $phone_arr);
        }
        if ($data['email'] && $data['email']!='')
        {
          $email_arr = array(
            'id'=>1494827,
            'values'=>array(
              array(
                'value'=>$data['email'],
                'enum'=>'WORK',
              )
            )
          );
          array_push($contacts['request']['contacts']['update'][0]['custom_fields'], $email_arr);
        }   
      }

      $link='https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/contacts/set';

      $resp = $this->execQuery($link, $contacts, 'POST'); 
      
      return $resp[0]==null ? array(null, $resp[1]) : $resp;
    }

    public function createTask($contact_id, $text)
    {
      $date = new DateTime();
      $tasks['request']['tasks']['add']=array(
        array(
          'element_id'=>$contact_id,
          'element_type'=>1,
          'task_type'=>3,
          'text'=>$text,
          'responsible_user_id'=>$this->responsible_user_id,
          'complete_till'=>$date->getTimestamp()
        )
      );

      $link='https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/tasks/set';

      $resp = $this->execQuery($link, $tasks, 'POST'); 

      return $resp[0]==null ? array(null, $resp[1]['tasks']['add']) : $resp;
    }

    private function execQuery($link, $data, $method='GET')
    {
      $curl=curl_init(); 
      curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
      curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
      curl_setopt($curl,CURLOPT_URL,$link);
      curl_setopt($curl,CURLOPT_HEADER,false);
      curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
      curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
      curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
      curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
      if ($method=='POST')
      {
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST'); 
        curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($data)); 
        curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json')); 
      }
       
      $out=curl_exec($curl); 
      $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);

      $code=(int)$code;
      
      if ($this->validateCode($code)=='OK') {
        $resp = json_decode($out,true);
        return array(null, $resp['response']);
      } 
      else {
        return $this->validateCode($code);
      }  
    }
  }
?>