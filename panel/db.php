<?
  class MyDB extends SQLite3
  {
    function __construct()
    {
      $this->open(dirname(__FILE__).'/db.db');
      $this->exec("CREATE TABLE IF NOT EXISTS landing (name TEXT, value TEXT, UNIQUE(name) ON CONFLICT REPLACE)");
    }

    function get($name)
    {
      $statement = $this->prepare("SELECT value FROM landing WHERE name = :name");
      $statement->bindValue(':name', $name);
      $result = $statement->execute();
      $row = $result->fetchArray();
      return $row['value'];
    }

    function set($name, $value)
    {
      if (empty($name) || is_null($value))
        return false;
      $statement = $this->prepare("INSERT INTO landing (name, value) VALUES (:name, :value)");
      $statement->bindValue(':name', $name);
      $statement->bindValue(':value', $value);
      return $statement->execute();
    }
  }
  $db = new MyDB();

  if (isset($_POST['get_orders'])){
    preg_match("/id:(\d{8,9})/", $db->get('metrika'), $yaId);
    echo $db->get('orders').'||'.$yaId[1];
  }
?>