<?
  $zip = new ZipArchive();
  $domain = $_SERVER['SERVER_NAME'];
  $filename = $domain."_".date('m.d.Y_H:i').'.zip';
  $root = basename(dirname(getcwd()));

  if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
    exit("Невозможно открыть <$filename>\n");
  }

  $rootPath = '../';
  $files = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($rootPath),
      RecursiveIteratorIterator::LEAVES_ONLY
  );

  foreach ($files as $name => $file)
  {
    if (!$file->isDir())
    {
      $filePath = $file->getRealPath();
      preg_match("/(".$root.".*)/", $filePath, $new_path);
      if (!preg_match("/[a-zа-я0-9.]_[0-9._:]{16}\.zip/", basename($file)))
      {
        $zip->addFile($filePath, $new_path[1]);
      }
    }
  }
  $zip->close();
?>