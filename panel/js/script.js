$( document ).ready(function() {

  $(".backup").on("click", function (e) {
    $(this).addClass('secondary loading disabled').removeClass('backup');
    $.ajax({
      type: "GET",
      url: "backup.php",
      success: function (data) {
        location.reload();
      }
    });
  });

  $(".del-file").on("click", function (e) {
    var file = $(this).data('file');
    $.ajax({
      type: "POST",
      data: { delfile: file },
      url: "delete.php",
      success: function (data) {
        location.reload();
      }
    });
  });
  
  $("header").sticky({
    topSpacing: 0
  });

  $('#trust_file').change(function() {
    $(this).parent().find('span').text($(this).val()!='' ? $(this).val().replace('C:\\fakepath\\','') : 'Файл не выбран');
  });

  $('#up').click(function() {
    return $('html, body').animate({
      scrollTop: 0
    }, 800);
  });

  $('.gen-utm').click(function() {
    var link = 'http://alpham.ru/?utm_source=client_site&utm_medium='+window.location.hostname.replace(/\./g,'_')+'&utm_campaign=made_by'
    $(this).parent().find('input').val(link);
  });
});