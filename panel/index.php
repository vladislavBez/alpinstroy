<?
  include 'db.php';
  $lifetime=300;
  $panel_path = '/panel';
  $site_path = '/';
  session_start();
  setcookie(session_name(),session_id(),time()+$lifetime);
  if (isset($_POST['login']) && isset($_POST['password'])){
    if ($_POST['login'] == 'admin' && hash('sha512', $_POST['password']) == '0323d23055fe9455a6c70710c38092677c49a5853b8df7adaef8ce680de21414017279aba4ae9a4b16ea45e38d3b5908123463b8ee5244254c33eeb1c9946aae'){
      $_SESSION['authorized'] = true;
      header('Location: '.$panel_path);
      die();
    }
    else{
      header('Location: '.$panel_path);
      die();
    }
  }
  if (isset($_GET['logout'])){
    unset($_SESSION['authorized']);
    header('Location: '.$panel_path);
  }
  if (!isset($_SESSION['authorized'])) {
    echo file_get_contents('login.html');
    die();
  } else {
    if (isset($_POST['post'])){
      foreach ($_POST['request'] as $key => $value) {
        $db->set($key, $value);
      }
    }

    $files = array();
    foreach ( scandir("../") as $filename ) {
      $files[] = $filename;
    }
    $backs = array();
    foreach ( scandir(getcwd()) as $filename ) {
      $backs[] = $filename;
    }
    if(isset($_FILES['trust_file']) && !empty($_FILES['trust_file']['name'])) {
      $finfo = new finfo(FILEINFO_MIME_TYPE);
      $inf = $finfo->file($_FILES['trust_file']['tmp_name']);
      if (false === $ext = array_search(
        $inf, array('html' => 'text/html','txt' =>'text/plain',
        ),
        true
      )){

      } else{
        if($_FILES['trust_file']['error'] == 0) {
          $uploadfile = '../'.basename($_FILES['trust_file']['name']);
          move_uploaded_file($_FILES['trust_file']['tmp_name'], $uploadfile);
        }
      }
    }

    if (isset($_POST['post'])) 
      header('Location: '.$panel_path);
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Панель</title>
  <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="./css/font-awesome.css">
  <link rel="stylesheet" href="./css/normalize.css">
  <link rel="stylesheet" href="./css/style.css">
</head>
<body>
  <div class="main-wrapper">
    <header>
      <div class="content-container">
        <img src="./img/logo2.png" alt="" class="logo">
        <div class="controls">
          <label>Панель сайта</label>
          <a href="<?=$site_path?>" target="_blank">Перейти на сайт</a>
          <a href="?logout" class="exit">Выйти</a>
        </div>
      </div>
      <div class="clearfix"></div>
    </header>
    <div class="main">
      <div class="content-container">
        <form method="POST" enctype="multipart/form-data">
          <input type="hidden" name="post" value="1">
          <div class="field">
            <label>Title</label>
            <input type="text" class="text-input" name="request[title]" value="<?=$db->get('title')?>">
          </div>
          <div class="field">
            <label>Description</label>
            <input type="text" class="text-input" name="request[description]" value="<?=$db->get('description')?>">
          </div>
          <div class="field">
            <label>Keywords</label>
            <input type="text" class="text-input" name="request[keywords]" value="<?=$db->get('keywords')?>">
          </div>
          <div class="field">
            <label>Ссылка на сайт alpham.ru</label>
            <div class="wrapper">
              <input type="text" class="text-input" name="request[marketing_link]" value="<?=$db->get('marketing_link')?>">
              <i class="fa fa-bolt fa-fw gen-utm"></i>
            </div>
          </div>
          <div class="field">
            <label>Основной email для заявок</label>
            <div class="checkbox">
              <input type="hidden" name="request[email_high_priority]" value='0'>
              <input type="checkbox" name="request[email_high_priority]" id="email_high_priority" value="1" <?=(bool)$db->get('email_high_priority') ? 'checked' : '' ?> >
              <label for="email_high_priority"><span></span>Задать письму высокий приоритет</label>
            </div>
            <input type="text" class="text-input" name="request[email]" value="<?=$db->get('email')?>">
          </div>
          <div class="field">
            <label>Дополнительные email для заявок(копии) через запятую</label>
            <input type="text" class="text-input" name="request[emailcc]" value="<?=$db->get('emailcc')?>">
          </div>
          <div class="field">
            <label>Email для политики конфиденциальности</label>
            <input type="text" class="text-input" name="request[email_conf]" value="<?=$db->get('email_conf')?>">
          </div>
          <div class="field">
            <label>Режим тестирования заявок(Все заявки будут приходить ТОЛЬКО на указанный email)</label>
            <div class="checkbox">
              <input type="hidden" name="request[test_mode]" value="0">
              <input type="checkbox" name="request[test_mode]" id="test_mode" value="1" <?=(bool)$db->get('test_mode') ? 'checked' : '' ?> >
              <label for="test_mode"><span></span>Включен</label>
            </div>
            <input type="text" class="text-input" name="request[test_email]" value="<?=$db->get('test_email')?>">
          </div>
          <div class="field">
            <label>Yandex метрика</label>
            <textarea name="request[metrika]"><?=$db->get('metrika')?></textarea>
          </div>
          <div class="field">
            <label>Google аналитика</label>
            <textarea name="request[analytics]"><?=$db->get('analytics')?></textarea>
          </div>
          <div class="field">
            <label>Цели</label>
            <textarea name="request[orders]"><?=$db->get('orders')?></textarea>
          </div>
          <div class="field">
            <label>Google\Yandex файлы</label>
            <div class="file-input">
              <input type="file" name="trust_file" id="trust_file">
              <label class="button gray" for="trust_file">Выберите файл</label>
              <span class="value">Файл не выбран</span>
            </div>
            <div>Текущие файлы:</div>
            <ul>
              <? foreach ($files as $file): ?>
                <?if (preg_match("/(yandex|google)_?[0-9a-z]{16}\.(html|htm|txt)/", $file)):?>
                  <li><?=$file?> &nbsp;<i title="Удалить файл" class="fa fa-trash del-file" data-file="<?=$file?>"></i></li>
                <?endif;?>
              <? endforeach; ?>
            </ul>
          </div>
          <div class="field" style="margin-top: 60px">
            <label>Бекапы</label>
            <div class="ui black button backup">Сделать новый бекап</div>
            <ul style="list-style-type: decimal;">
              <? foreach ($backs as $file): ?>
                <?if (preg_match("/[a-zа-я0-9.]_[0-9._:]{16}\.zip/", $file)):?>
                  <li><?=$file?> | <a href="<?=$file?>"><i title="Скачать бекап" class="fa fa-download"></i></a> | <i title="Удалить бекап" class="fa fa-trash del-file" data-file="<?=$file?>"></i> | <?=round(filesize($file)/1024/1024, 2)?>МБ</li>
                <?endif;?>
              <? endforeach; ?>
            </ul>
          </div>
          <br>
          <input type="submit" class="save-button button" value="Сохранить">
        </form>
        <div id="up"></div>
      </div>
    </div> 
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="./js/jquery.sticky.js"></script>
  <script src="./js/smoothScroll.js"></script>
  <script src="./js/script.js"></script>
</body>
</html>